#Basic Harpjs setup

To install after cloning;

`npm i`

To run harp server;

`npm run dev`

Navigate to http://localhost:9000

Project structure;
```
|-index.html (based on Html5 boilerplate)
|-JS/
   |-main.js  
|-CSS/ 
   |-main.scss 
```